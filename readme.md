# docker-compose-merge

## Installation

```bash
npm i -g docker-compose-merge
```

## Usage

```bash
docker-compose-merge -i docker-compose.override.yml.yml docker-compose.override.yml [-o file_output]
```

## Requirements
- node
- npm

## License
[MIT](https://choosealicense.com/licenses/mit/)