/*
 NodeJS implementation of
 https://docs.docker.com/compose/extends/#adding-and-overriding-configuration
*/

const jsYaml = require('js-yaml'),
    fs = require('fs'),
    deepmerge = require('deepmerge')

function mergeDefault(base, extend){
  if(typeof extend === 'object'){
    if(base === undefined){
      base = {}
    }
    if(extend === null){
      extend = {}
    }
    return deepmerge(base, extend)
  }
  else{
    return extend
  }
}

function mergeParseEqual(base, extend){
  if(base === undefined){
    base = {}
  }
  if(extend === null){
    extend = {}
  }
  if(Array.isArray(extend)){
    for(const value of extend){
      const splitIndex = value.indexOf('=')
      const key = value.slice(0,splitIndex).trim()
      const val = value.slice(splitIndex+1).trim()
      base[key] = val
    }
  }
  else{
    base = deepmerge(base, extend)
  }
  return base
}

function mergeParseVolume(base, extend){
  if(base === undefined){
    base = []
  }
  if(extend === null){
    extend = []
  }
  for(const value of extend){
    const [val, key] = value.split(':')
    const index = base.findIndex((item)=>{
      const [bval, bkey] = item.split(':')
      return bkey===key
    })
    if(index !== -1){
      base[index] = value
    }
    else{
      base.push(value)
    }
  }
  return base
}

function mergeServices(base, extend) {
  for(const [key, value] of Object.entries(extend)){
    const src = base[key] || {}
    if(value === null){
      value = {}
    }
    base[key] = mergeService(src, value)
  }
  return base
}
function mergeService(base, extend) {
  for(const [key, value] of Object.entries(extend)){
    switch(key){
      case 'environment':
      case 'labels':
        base[key] = mergeParseEqual(base[key], value)
        break
      case 'volumes':
      case 'devices':
        base[key] = mergeParseVolume(base[key], value)
        break
      default:
        base[key] = mergeDefault(base[key], value)
        break
    }
  }
  return base
}

function mergeYaml(filesArray) {
  const base = {}
  for(let file of filesArray){
    const parsedConfig = jsYaml.safeLoad(fs.readFileSync(file, 'utf8'))
    for(let [key, value] of Object.entries(parsedConfig)){
      const src = base[key] || {}
      if(typeof value == 'object'){
        if(value === null){
          value = {}
        }
        switch(key){
          case 'services':
            base[key] = mergeServices(src, value)
            break
          default:
            base[key] = deepmerge(src, value)
            break
        }
      }
      else{
        base[key] = value
      }
    }
  }
  return base
}

module.exports = mergeYaml